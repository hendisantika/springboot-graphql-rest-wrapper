package com.hendisantika.springbootgraphqlrestwrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGraphqlRestWrapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootGraphqlRestWrapperApplication.class, args);
    }

}
