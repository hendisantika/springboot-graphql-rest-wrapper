# Wrap REST API with GraphQL in Spring Boot

## Introduction

This post will show you how to wrap REST API with [GraphQL](https://graphql.org) in Spring Boot framework. 
So here we will create a Java based GraphQL server. When you come to know that 
GraphQL has advantages over REST API and you want to start using GraphQL 
but you are stuck with legacy REST APIs. Then this tutorial may help you to wrap 
your existing REST API with GraphQL. Hence you don’t need to rewrite your existing 
REST APIs into GraphQL but you can utilize them with GraphQL.

[GraphQL](https://graphql.org) is an open source query language introduced by Facebook 
in the year of 2015. I would not explain here about GraphQL as Facebook has already 
explained in details and can be found [here](https://www.howtographql.com/basics/0-introduction/) . You will also find the differences 
between REST and GraphQL.

You may also like to read Wrap REST API with GraphQL in Node.js.

In this Spring Boot application we will also build a GraphQL IDE or 
Playground for performing query and mutation on GraphQL API. 
GraphQL IDE is similar to Postman or REST client but GraphQL IDE is used for GraphQL API, 
whereas Postman or REST client is used for REST API.

You can use any technology for implementing GraphiQL server. 
Here we will use Spring Boot and Java to implement GraphQL server.

## Prerequisites

Knowledge on GraphQL, REST, Spring Boot and Java

IntelliJ IDEA / Eclipse Neon or any IDEA, Spring Boot 2.2.1, Java 1.8, GraphQL 11.0

## Identifying REST Endpoints

The first task is to identify the existing REST end-points and the example 
we are using here Spring Boot Data JPA CRUD Example has the following endpoints:
```
GET /websites
GET /website/{id}
POST /website
PUT /website
DELETE /website
```

The endpoint – `GET /websites` – lists all the available website information from the server.

The endpoint – `GET /website/{id}` – lists the available website information for a given website id from the server.

The endpoint – `POST /website` – adds a new website information to the server. The website information is passed in the body as a JSON payload.

The endpoint – `PUT /website` – updates an existing website information to the server. The website information is passed in the body as a JSON payload.

The endpoint – `DELETE /website` – deletes an existing website information from the server. The website information is passed in the body as a JSON payload. 

## Identifying Data Model and Creating Domain Class

```
id, name, url
```

The id indicates the Id of the website, name indicates the name of the website and url indicates the URL of the website.

So we can create corresponding VO class that will help us to easily convert to JSON payload or vice-versa.

Below is the Domain class that will represent a website information.
```
public class Website {
	private Integer id;
	private String name;
	private String url;
	public Website() {
	}
	public Website(Integer id, String name, String url) {
		this.id = id;
		this.name = name;
		this.url = url;
	}
	//getters and setters
}
```

## Defining GraphQL Schema

We are creating a new file **schema.graphql** in *src/main/resources* with the following content:
```
type Query {
  websites: [Website!]!
  website(id: ID!): Website
}
type Mutation {
  addWebsite(name: String!, url: String!): String!
  updateWebsite(id: ID!, name: String!, url: String!): String!
  deleteWebsite(id: ID!): String!
}
type Website {
  id: ID
  name: String
  url: String
}
```

This schema defines top level fields (in the type **Query**): **websites** 
which returns the details of all available websites and website that returns 
the details of a particular website for a given id.

It also defines the **Website** which has fields id, name and url.

> The Domain Specific Language shown above which is used to describe a schema 
is called Schema Definition Language or SDL. More details about it can be found [here](https://graphql.org/learn/schema/).

We know that Query is used to query GraphQL server for fetching or reading data.

We also need to save new website information, update existing website information or delete existing website information. 
Therefore we used **Mutation** to perform create, update and delete operations.

In the above Query, we have used **[]** and it means it will return a list of websites.

Notice we have also used **!** to denote that the parameter is required. If you make a parameter required and if you do not pass value or get value from server on this parameter then GraphQL will throw errors.

## Creating GraphQL DataFetcher

The most important concept for a GraphQL Java server is a DataFetcher that fetches the Data for one field when a query is executed.

While GraphQL Java is executing a query, it calls the appropriate DataFetcher for each field it encounters in query. A DataFetcher is an Interface with a single method, taking a single argument of type DataFetcherEnvironment:
```
public interface DataFetcher<T> {
    T get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception;
}
```

Every field from the schema has a DataFetcher associated with. If you don’t specify any DataFetcher for a specific field, then the default PropertyDataFetcher is used.

Now we will create a class GraphQLDataFetcher that will fetch data from REST API using Spring’s RestTemplate.

If you look at the methods in the below class, then you will find that any method returns a DataFetcher implementation which takes a DataFetcherEnvironment and returns an appropriate object.

## Creating GraphQL Provider

We had create GraphQL schema, now it’s also important to create a GraphQL provider class to read and parse the schema file for performing query and mutation.

In the below code the init() method creates GraphQL instance.

We use Spring Resource to read the file from our classpath, then create a GraphQLSchema and GraphQL instance. This GraphQL instance is exposed as a Spring Bean via the graphQL() method annotated with @Bean. The GraphQL Java Spring adapter will use that GraphQL instance to make our schema available via HTTP on the default url /graphql.

We implement the buildSchema() method which creates the GraphQLSchema instance and wires in code to fetch data.

TypeDefinitionRegistry is the parsed version of our schema file. SchemaGenerator combines the TypeDefinitionRegistry with RuntimeWiring to actually make the GraphQLSchema.

buildRuntimeWiring uses the dataFetcher bean to actually register DataFetchers for performing Query and Mutation.

## Testing the Application

> Make sure that your application Spring Boot Data JPA CRUD Example is running on server.

Now run the above main class, your application will be deployed into embedded tomcat server on port 9000.

Your GraphQL endpoint, by default, will point to http://localhost:8080/graphql.

Using the above URL you can only execute Query but you won’t be able to execute Mutation.

So if you want to execute Query using the default GraphQL URL then you can execute below URLs for fetching all available websites and a particular website, respectively:
```
http://localhost:8080/graphql?query=%7Bwebsites%7Bid%20name%20url%7D%7D
http://localhost:8080/graphql?query=%7Bwebsite%28id:1%29%7Bid%7D%7D
```

The above URLs are encoded ones because HTTP request won’t allow you to use some characters. The equivalent human readable URLs would be as follows:
```
http://localhost:8080/graphql?query={websites{id name url}}
http://localhost:8080/graphql?query={website(id:1){id}}
```
Therefore, we need GraphQL IDE or Playground that will help us to execute Query and Mutations with our standard input formats. The next section explains how to create a GraphQL IDE or Playground.

Naturally when you have made changes to the application, you need to restart your server.

Once you restart server, you can point the URL at http://localhost:8080 in the browser and you will see the following screen on the browser.

So now it’s very easy to execute Query or Mutation on this IDE.
wrap rest api with graphql

### Reading Website Information

Now when we execute below Query on left pane then we get nothing on the right pane because there is no available website in the server.
wrap rest api with graphql in spring boot

### Creating Website Information

Now execute Mutation to save new website information.
wrap rest api with graphql
wrap rest api with graphql

### Reading Website Information

Now execute Query to fetch all available websites.
wrap rest api with graphwl

You can also query a particular website as shown below.
wrap rest api with graphql

### Updating Website Information

You can update an existing website information.
wrap rest api in graphql

### Deleting Website Information

You can delete an existing website information.
rest api with graphql

### Reading Website Information

The final Query output will be as shown below:
graphql server

That’s all. Hope you got idea on how to wrap REST API with GraphQL in Spring Boot application.